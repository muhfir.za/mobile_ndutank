package com.example.irfan.squarecamera;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnCamerad;
    private Button btnReset;
    private Button btnValidation;
    databaseHelper myDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDB = new databaseHelper(this);
        final EditText editText = (EditText)findViewById(R.id.nrp);
        btnCamerad = (Button) findViewById(R.id.btn_camerad);
        btnReset = (Button) findViewById(R.id.btn_reset);
        btnValidation = (Button) findViewById(R.id.btn_validation);
        btnCamerad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nrp = editText.getText().toString().trim();
                Intent intent = new Intent(MainActivity.this, CameradActivity.class);
                intent.putExtra("nrp",nrp);
                startActivity(intent);
            }
        });

        btnValidation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nrp = editText.getText().toString().trim();
                Intent intent = new Intent(MainActivity.this, ValidationActivity.class);
                intent.putExtra("nrp",nrp);
                startActivity(intent);
            }
        });


        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDB.resetAllData();
                Toast.makeText(MainActivity.this, "Deleted!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
