package com.example.irfan.squarecamera;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class databaseHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "time.db";
    public static final String TABLE_NAME = "timecounter";
    public static final String COL_1 = "NRP";
    public static final String COL_2 = "counter";
    public static final String COL_3 = "time";

    public databaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_NAME + "(NRP STRING , COUNTER INTEGER, TIME LONG)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public boolean insertData (String nrp, Integer counter, long time){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, nrp);
        contentValues.put(COL_2, counter);
        contentValues.put(COL_3, time);
        long result = db.insert(TABLE_NAME, null, contentValues);
        if (result == -1)
            return false;
        else return true;
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME, null);
        return res;
    }

    public void resetAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+TABLE_NAME);
    }

    public Cursor getAverage(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select sum(time)/count(*) from "+TABLE_NAME, null);
        return res;
    }

}
